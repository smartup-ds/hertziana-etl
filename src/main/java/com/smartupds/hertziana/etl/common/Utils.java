
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.hertziana.etl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author mafragias
 */
public class Utils {
    /**
     * Matching patterns of objects included in adjacent spaces.
     * @param txt input string
     * @return the content of the quotes
     */
    public static HashSet<String[]> matchAdjacentObjects(String txt){
        HashSet<String[]> list = new HashSet<>();
        txt = txt.trim().replace("\n", "");
        Matcher adjacent = Pattern.compile("([a-zA-Z,\\s]*):").matcher(txt);
        String group = null;
        String elmts = null;
        int start = 0;
        int end = 0;
        while(adjacent.find()) {
            end = adjacent.start();
            elmts = txt.substring(start,end);
//            System.out.println(elmts.trim());
            if (group!=null && elmts!=null)
                list.add(new String[]{elmts,group});
            group = adjacent.group(1).trim();
            start = adjacent.end();
        }
        elmts = txt.substring(start);
//        System.out.println(elmts.trim());
        list.add(new String[]{elmts,group});
        return list;
    }
    
        /**
     * Matching patterns of objects included in adjacent spaces.
     * @param txt input string
     * @return the content of the quotes
     */
    public static ArrayList<String> matchNumberObjects(String txt){
        ArrayList<String> list = new ArrayList<String>();
        Matcher objects = Pattern.compile("([0-9]+)[\\s]*\\.").matcher(txt.trim().replace("\n", ""));
        if(objects.find()) {
            list.add(objects.group(1));
            list.add(txt.trim().replace("\n", "").substring(objects.end()));
        }
        return list;
    }
        /**
     * Matching patterns of objects included in adjacent spaces.
     * @param txt input string
     * @return the content of the quotes
     */
    public static ArrayList<String> matchParenthesis(String txt){
        ArrayList<String> list = new ArrayList<String>();
        Matcher objects = Pattern.compile("(.*)\\((.*)\\)").matcher(txt.trim().replace("\n", ""));
        if(objects.find()) {
            list.add(objects.group(1));
            list.add(objects.group(2));
        }
        return list;
    }
    /**
     * Matching patterns of double dots.
     * @param txt input string
     * @return the content of the quotes
     */
    public static ArrayList<String> matchDoubleDots(String txt){
        ArrayList<String> list = new ArrayList<String>();
        txt = txt.trim();
        Matcher objects = Pattern.compile("([0-9a-z_]*)[\\s]*:").matcher(txt);
        int start=0,end=0;
        while(objects.find()) {
            if (end!=0){
                list.add(txt.substring(end,objects.start()).trim().replace("\n",""));
            }
            list.add(objects.group(1));
            end = objects.end();
            
        }
        list.add(txt.substring(end,objects.regionEnd()).trim().replace("\n",""));
        return list;
    }
    /**
     * Matching patterns of double dots.
     * @param txt input string
     * @return the content of the quotes
     */
    public static ArrayList<String> matchObjectDimensions(String txt){
        ArrayList<String> list = new ArrayList<String>();
        txt = txt.trim();
        Matcher objects = Pattern.compile("([a-zA-Z\\s]*)\\(([0-9a-zA-Z_]*)\\):").matcher(txt);
        int start=0,end=0;
        while(objects.find()) {
            if (end!=0){
                list.add(txt.substring(end,objects.start()).trim().replace("\n",""));
            }
            list.add(objects.group(1).trim());
            list.add(objects.group(2).trim());
            end = objects.end();
            
        }
        list.add(txt.substring(end,objects.regionEnd()).trim().replace("\n",""));
        return list;
    }
    
    /**
     * Matching patterns of double dots.
     * @param txt input string
     * @return the content of the quotes
     */
    public static ArrayList<String> matchDimensions(String txt){
        ArrayList<String> list = new ArrayList<String>();
        txt = txt.trim();
        Matcher height = Pattern.compile("(altezza|height)[\\s]*:[\\s]*([0-9]*)").matcher(txt);
        Matcher width = Pattern.compile("(larghezza|width)[\\s]*:[\\s]*([0-9]*)").matcher(txt);
        Matcher depth = Pattern.compile("(profondità|depth)[\\s]*:[\\s]*([0-9]*)").matcher(txt);
        
        if(height.find()){
            list.add("height");
            list.add(height.group(2));
        }
        
        if(width.find()){
            list.add("width");
            list.add(width.group(2));
        }
        
        if(depth.find()){
            list.add("depth");
            list.add(depth.group(2));
        }
        
        
        return list;
    }
}