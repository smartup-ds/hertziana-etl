/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.hertziana.etl.common;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author mafragias
 */
public class Resources {

    public static final String OBJECT = "Object";
    public static final ArrayList<String> MOTHERFIELDS = new ArrayList<String>(
            Arrays.asList(new String[]{ "filename", "Monument_conservation_site","Current_Title",
                                        "Historical_Title","Image_exterior","Caption_exterior",
                                        "Image_interior","Caption_interior","Image_plan",
                                        "Caption_plan","Location_gps","State",
                                        "Region", "Province","Town",
                                        "Place","Address","Ecclesiastical_jurisdiction",
                                        "Heritage","Architecture_Type","Historical_Overview","Includes",
                                        "Map_Image","Other_objects_in_storage_or","Other_objects_in_monument",
                                        "Lost_objects","Other_places_of_provenance_of_the_objects","Essential_bibliography",
                                        "Keywords","Notes","Editors",
                                        "Date","Credits"}));
    public static final ArrayList<String> CHILDFIELDS = new ArrayList<String>(
            Arrays.asList(new String[]{ "Child_ID","Title","Image_child","Caption_child","Image_child_other","Image_child_other_caption",
                                        "Resource_type","Condition_PRESERVED","Condition_IN_SITU","Condition_DECONTESTUALIZZATO",
                                        "Condition_RIFUNZIONALIZZATO","Condition_PERDUTO","Monument_conservation_site","placement_in_schema",
                                        "Image_child_plan","Provenance","original_location_unknown","Heritage",
                                        "Author","Short_description","History","Historic-artistic_analysis",
                                        "Archaeological_observation","Chronology","Chronology_plain","chronological_references",
                                        "Epigraphy","Physical_dimension","Techniques","Materials","Conservation_notes",
                                        "Visual_sources_related_to_the_object","Components","Map_Image","Connected_decontextualized_components",
                                        "Documented_but_lost_components","suggested_relationships","Essential_bibliography",
                                        "Keywords","Notes", "Editors","Last_Updated",
                                        "Credits"}));
    public static final ArrayList<String> GRANDCHILDFIELDS = new ArrayList<String>(
            Arrays.asList(new String[]{ "Grandchild_ID","Title","Image_grandchild","Caption_grandchild","Image_grandchild_other","Image_grandchild_other_caption",
                                        "Resource_type","Condition_PRESERVED","Condition_IN_OPERA","Condition_IN_SITU",
                                        "Condition_funzione_originaria","Condition_DECONTESTUALIZZATO","Condition_RIFUNZIONALIZZATO","Condition_PERDUTO",
                                        "Monument_conservation_site","placement_in_schema","Image_grandchild_plan", "Provenance",
                                        "original_location_unknown","is_part_of","position_within_the_pertaining_object","Image_grandchild_plan_localization",
                                        "Heritage", "Author","Short_description","History",
                                        "Historic-artistic_analysis", "Archaeological_observation","Chronology","Chronology_plain",
                                        "chronological_references", "Epigraphy","Physical_dimension","Techniques","Materials",
                                        "Scientific_Analysis","Ornamental_patterns", "Conservation_notes","Visual_sources_related_to_the_object",
                                        "suggested_relationships","Essential_bibliography", "Keywords","Notes",
                                        "Editors","Last_Updated","Credits"}));
    public static final String HEIGHT = "height";
    public static final String WIDTH = "width";
    public static final String CONDITION = "Condition";
    public static final String MONUMENT_ID = "Monument_ID";
    public static final String CHILD_ID = "Child_ID";
    public static final String GRANCHILD_ID= "Grandchild_ID";
    public static final String DIAMETER = "diameter";
    public static final String DEPTH = "depth";
    
    
}
