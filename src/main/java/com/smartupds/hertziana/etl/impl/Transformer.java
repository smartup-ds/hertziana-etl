/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.hertziana.etl.impl;

import com.smartupds.hertziana.etl.common.Resources;
import com.smartupds.hertziana.etl.common.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.dom4j.Document;
import org.dom4j.Element;

/**
 *
 * @author mafragias
 */
public class Transformer {
    Document doc;
    int count=1;
    private int aliasCount=1;
    public Transformer(Document doc){
        this.doc = doc;
    }
    
    
    public void transformMother() {
        Element rootElement = doc.getRootElement();
        List<Element> elements = rootElement.elements();
        for(Element element : elements) {
            if (element.element("filename") == null) {
                addFilenameElement(element);
            }
            addAliasElement(element);
            addDocumentIdAsAttribute(element.element("filename"));
            split(element.element("Includes"),Resources.OBJECT);
//            element.element("Includes").elements().forEach(elmt -> addIncludesIdAsAttribute(elmt));
//            specialSplit(element.element("Other_objects_in_adjacent_spaces"),Resources.OBJECT);
            specialSplit(element.element("Other_objects_in_monument"),Resources.OBJECT);
            splitElement(element.element("Lost_objects"),";",Resources.OBJECT);
            splitElement(element.element("Editors"),",","Editor");
            specialSplit(element.element("Other_places_of_provenance_of_the_objects"),Resources.OBJECT);
            images(element.element("Image_exterior"));
            images(element.element("Image_interior"));
            images(element.element("Image_plan"));
            images(element.element("Map_Image"));
        }
    }
    public void transformMothers() {
        Element rootElement = doc.getRootElement();
        List<Element> elements = rootElement.elements();
        for(Element element : elements) {
            splitElement(element.element("Editors"),",","Editor");
            addFilenameElement(element);
            addAliasElement(element);
            addDocumentIdAsAttribute(element.element("filename"));
        }
    }
        
    public void transformChild() {
        Element rootElement = doc.getRootElement();
        List<Element> elements = rootElement.elements();
        for(Element element : elements) {
            seperateSpecial(element.element("Provenance"),"Place");
            splitAtDoubleDots(element.element("Components"),"Component");
//            splitAtDoubleDots(element.element("Connected_decontextualized_components"),"Component");
            splitAtDoubleDots(element.element("Documented_but_lost_components"),"Component");
            splitDimensions(element.element("Physical_dimension"));
            specialSplit(element.element("Connected_decontextualized_components"),Resources.OBJECT);
            splitElement(element.element("Editors"),",","Editor");
            condition(element.element("Condition_PRESERVED"));
            condition(element.element("Condition_IN_SITU"));
            condition(element.element("Condition_DECONTESTUALIZZATO"));
            condition(element.element("Condition_RIFUNZIONALIZZATO"));
            condition(element.element("Condition_PERDUTO"));
            images(element.element("Image_child"));
            images(element.element("Image_child_other"));
            images(element.element("Image_child_plan"));
            images(element.element("Map_Image"));
        }
    }

    public void transformGrandChild() {
        Element rootElement = doc.getRootElement();
        List<Element> elements = rootElement.elements();
        for(Element element : elements) {
            seperateSpecial(element.element("Provenance"),"Place");
            addIdAsAttribute(element.element("is_part_of"));
            splitDimensions(element.element("Physical_dimension"));
            splitElement(element.element("Editors"),",","Editor");
            condition(element.element("Condition_PRESERVED"));
            condition(element.element("Condition_IN_OPERA"));
            condition(element.element("Condition_IN_SITU"));
            condition(element.element("Condition_funzione_originaria"));
            condition(element.element("Condition_DECONTESTUALIZZATO"));
            condition(element.element("Condition_RIFUNZIONALIZZATO"));
            condition(element.element("Condition_PERDUTO"));
            images(element.element("Image_grandchild"));
            images(element.element("Image_grandchild_other"));
            images(element.element("Image_grandchild_plan"));
            images(element.element("Image_grandchild_plan_localization"));
        }
    }
    private void split(Element element,String newChildName){
        String content = element.getText();
        element.clearContent();
        Matcher m = Pattern.compile("([0-9]+)\\s*[.|:]\\s*(.*)").matcher(content);
        while(m.find()){
//            System.out.println(m.group(1) + " - " + m.group(2));
            element.addElement(newChildName).addText(m.group(2)).addAttribute("id",String.format("%02d", Integer.parseInt(m.group(1))));
        }
    }
    private void splitElement(Element element,String del,String newChildName){
        if (element!=null){
            String content = element.getText();
            String[] split = content.split(del);
            element.clearContent();
            int id = 2000;
            for (String object:split){
    //                System.out.println("-------"+object.trim()+"-------");
                    String text = object.trim().replace("\n", "");
                    if (!text.equals("")){
                        ArrayList<String> list = Utils.matchNumberObjects(object.trim().replace("\n", ""));
                        if(newChildName.equals(Resources.OBJECT)){
                            if (!list.isEmpty())
                                element.addElement(newChildName).addText(list.get(1)).addAttribute("id",String.format("%02d", Integer.parseInt(list.get(0))));
                            else
                                element.addElement(newChildName).addText(object.trim().replace("\n", "")).addAttribute("id", id +"");
                            id++;
                        } else
                            element.addElement(newChildName).addText(object.trim().replace("\n", ""));
                    }
            }
    //        System.out.println(element.asXML());
    //        System.out.println(Arrays.toString(split));
    //        element.addElement(newChildName);
        }
    }

    private void specialSplit(Element element, String newChildName) {
        if(element.hasMixedContent()){
            String content = element.getText();
            HashSet<String[]> splits = Utils.matchAdjacentObjects(content);
            element.clearContent();
            for (String[] split: splits) {
                String[] vals = split[0].split(";");
                for (String val:vals){
    //                System.out.println(val.trim());
                    ArrayList<String> numbers = Utils.matchNumberObjects(val);
                    element.addElement(newChildName).addText(numbers.get(1))
                            .addAttribute("id", String.format("%02d",Integer.parseInt(numbers.get(0))))
                            .addAttribute("belongs", split[1] +"")
                            .addAttribute("belongs_id", split[1].hashCode() +"");
                }
            }
        }
    }
    
    private void seperateSpecial(Element element, String newChildName) {
        if (element!=null && element.getText()!=null && !element.getText().equals("")){
            String content = element.getText();
            ArrayList<String> splits = Utils.matchParenthesis(content);
            element.clearContent();
            if (splits.size()>0){
                element.addElement(newChildName).addText(splits.get(0)).addAttribute("notes",splits.get(1));        
            } else {
                element.addElement(newChildName).addText(content);
            }
        }
    }

    private void splitAtDoubleDots(Element element, String newChildName) {
        String content = element.getText();
        if (!content.equals("")){
            element.clearContent();
            Matcher m = Pattern.compile("([0-9]+_[0-9]+)(.*)").matcher(content);
            while(m.find()){
                element.addElement(newChildName).addText(m.group(2) +"("+m.group(1)+")").addAttribute("id", m.group(1));
            }
            Matcher m2 = Pattern.compile("([0-9a-zA-Z_]+)\\s*[:|.](.*)").matcher(content);
            
            while (m2.find()){
                element.addElement(newChildName).addText(m2.group(2)+"("+m2.group(1)+")").addAttribute("id", m2.group(1));
            }
        }
    }
    
    private void splitDimensions(Element element){
        if (element!=null){
            String content = element.getText().replace("\n", " ");
            element.clearContent();
            if(!content.equals("")){
                Matcher m = Pattern.compile("([a-zA-Z]*)\\s*([0-9,]*)\\s*x\\s*([0-9,.]*)").matcher(content);
                int mend = 0;
                while(m.find()){
                    Element dim = element.addElement(Resources.OBJECT).addAttribute("unit",m.group(1));
                    dim.addElement(Resources.HEIGHT).addText(m.group(2));
                    dim.addElement(Resources.WIDTH).addText(m.group(3));
                    mend = m.end();
                }

                if (mend<content.length()){
                    int end = 0 ;
                    Matcher m2 = Pattern.compile("([\\w]+\\s*[^:])\\s*:\\s*([0-9,.]+)\\s*([a-zA-Z]+)").matcher(content);
                    while(m2.find()){
                        if (m2.group(1).equals("altezza")){ // height
                            Element dim = element.addElement(Resources.OBJECT).addAttribute("unit",m2.group(3));
                            dim.addElement(Resources.HEIGHT).addText(m2.group(2));
                        } else if (m2.group(1).equals("larghezza")){ // width
                            Element dim = element.addElement(Resources.OBJECT).addAttribute("unit",m2.group(3));
                            dim.addElement(Resources.WIDTH).addText(m2.group(2));
                        } else if (m2.group(1).equals("profondità")){ //depth
                            Element dim = element.addElement(Resources.OBJECT).addAttribute("unit",m2.group(3));
                            dim.addElement(Resources.DEPTH).addText(m2.group(2));             
                        } else if (m2.group(1).equals("diametro")){
                            Element dim = element.addElement(Resources.OBJECT).addAttribute("unit",m2.group(3));
                            dim.addElement(Resources.DIAMETER).addText(m2.group(2));   
                        }
                        end = m2.end();
                    }
                    element.addElement(Resources.OBJECT).setText(content.substring(end));
                }

    //    //        System.out.println(content);
    //            ArrayList<String> list = Utils.matchObjectDimensions(content);
    //    //        list.forEach(s->System.out.println(s));
    //            for (int i=0;i<list.size();i++){
    //    //            System.out.println(list.get(i+2).replace("\n", " "));
    //                ArrayList<String> dimensions = Utils.matchDimensions(list.get(i+2).replace("\n", " "));
    //                for(int j=0;j<dimensions.size();j++){
    //    //                System.out.println(dimensions.get(j));
    //                    element.addElement(Resources.OBJECT).addAttribute("name",list.get(i))
    //                                                        .addAttribute("id",list.get(i+1))
    //                                                        .addAttribute("unit","cm")
    //                                                        .addElement(dimensions.get(j))
    //                                                        .addText(dimensions.get(j+1));
    //                    j++;
    //                }
    //    ////            System.out.println(list.get(i)+" - "+list.get(i+1));
    //    //            element.addElement(Resources.OBJECT).addText(list.get(i+1)).addAttribute("id",list.get(i));
    //                i+=2;
    //            }
            }
        }
    }

    private void addIdAsAttribute(Element element) {
        if(element!=null && element.hasMixedContent()){
            String[] content = element.getText().trim().split("\\(");
            element.clearContent();
            element.addText(content[0]);
            element.addAttribute("id", content[1].replace(")", ""));
        }
    }

    private void addDocumentIdAsAttribute(Element element) {
        String[] content = element.getText().trim().split("_");
        element.addAttribute("id", content[0]+content[1]+String.format("%04d",count));
        count++;
    }

    private void addIncludesIdAsAttribute(Element element) {
        System.out.println(element.getTextTrim());
        String id = element.getTextTrim().substring(0, element.getTextTrim().indexOf("."));
        String content = element.getTextTrim().substring(element.getTextTrim().indexOf("."),element.getTextTrim().length());
        element.clearContent();
        element.addText(content);
        element.addAttribute("id", id);
    }

    private void addFilenameElement(Element element) {
        String region;
        if (element.elementText("Region").length()>3)
            region =element.elementText("Region").substring(0,3);
        else 
            region = element.elementText("Region");
        String content =  region + "_" 
                + element.elementText("Province") + "_"
                + element.elementText("Town") + "_"
                + element.elementText("Monument_conservation_site").trim().replace(" ", "_").replace(".", "");
        element.addElement("filename").setText(content.trim().replace(" ", "_").replaceAll("[^a-zA-Z0-9_]", "_").toUpperCase());
    }
    
    private void addAliasElement(Element element) {
        String region;
        if (element.elementText("Region").length()>3)
            region =element.elementText("Region").substring(0,3);
        else 
            region = element.elementText("Region");
        String content =  region + "_" 
                + element.elementText("Province") + "_"
                + String.format("%05d",aliasCount);
        element.addElement("alias").setText(content.trim().replace(" ", "_").replaceAll("[^a-zA-Z0-9_]", "_").toUpperCase());
        aliasCount++;
    }

    private void condition(Element element) {
        if (element!=null && !element.getTextTrim().equals("")){
            String content = element.getName().replace("Condition_", "")
                                                .replace("_", " ")
                                                .toLowerCase();
            element.clearContent();
            element.setName(Resources.CONDITION);
            element.setText(content);
        }
    }

    private void images(Element element) {
        if (element!=null){
            String monument_id = "";
            if (element.getParent().element("filename")!=null)
                monument_id = element.getParent().element("filename").getText();
            else if (element.getParent().element(Resources.MONUMENT_ID)!=null)
                monument_id = element.getParent().element(Resources.MONUMENT_ID).getText();
            if (element.getName().startsWith("Image")){
                element.setText(monument_id+"_"+element.getName().replace("Image_", "").toLowerCase()+".jpg");
            } else {
                element.setText(monument_id+"_plan_localization.jpg");
            }
        }
    }
}
