/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.hertziana.etl.impl;

import com.smartupds.hertziana.etl.common.Resources;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import java.io.File;  
import java.io.FileInputStream;  
import java.util.ArrayList;
import java.util.Iterator;  
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 *
 * @author mafragias
 */
public class Parser {

    private String path;
    
    public Parser(String path){
        this.path = path;
    }

    public Document parseXML() throws FileNotFoundException, DocumentException{
        SAXReader reader = new SAXReader();
        reader.setEncoding("UTF-8");
        Document doc = reader.read(new FileInputStream(path));
        return doc;
    }
    
    public Document parseTSVtoXML(){
        BufferedReader tsvReader = null;
        Document doc = DocumentHelper.createDocument();
        try {
            tsvReader = new BufferedReader( new InputStreamReader(new FileInputStream(path), "UTF8"));
            int rowCounter = 0;
            String row;
            int numOfFields = 0;
            // root element
            Element rootElement = doc.addElement("root");
            doc.setRootElement(rootElement);
            String[] fields = {};
            while ( (row = tsvReader.readLine()) != null ) {
                if (!row.trim().equals("") & !row.trim().startsWith("#")){
                    try {
                        if (rowCounter==0){
                            fields = row.split("\t");
                            numOfFields = row.split("\t").length;
                        }else{
                            Element resource = rootElement.addElement("row");
                            String[] data = new String[numOfFields];
                            for(int i=0; i<numOfFields; i++){
                                Element child = resource.addElement(fields[i]
                                        .replace("/", "o").replace("(", "").replace(")", "")
                                        .replace(" ", "_").replace("?", ""));
                                if(i<row.split("\t").length)
                                    child.setText(row.split("\t")[i]);
                                else
                                    child.setText("");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    rowCounter++;
                }
            }
//        XMLWriter xmlwriter = new XMLWriter( new OutputStreamWriter(new FileOutputStream(path.toAbsolutePath().toString()+".xml"), "UTF-8"), format );
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                tsvReader.close();
                
            } catch (IOException ex) {
                Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return doc;
    }

    public ArrayList<Document> parseExcelToXML() {
        ArrayList<Document> docs = new ArrayList<>();
        for(int i=0;i<3;i++)
            docs.add(DocumentHelper.createDocument());
        
        try {  
            File file = new File(this.path);   //creating a new file instance  
            FileInputStream fis = new FileInputStream(file);   //obtaining bytes from the file  
            //creating Workbook instance that refers to .xlsx file  
            XSSFWorkbook wb = new XSSFWorkbook(fis);  
            for(int i=0;i<3;i++){ // for each sheet
                Element rootElement = docs.get(i).addElement("root");
                docs.get(i).setRootElement(rootElement);
                Element resource = rootElement.addElement("row");
                XSSFSheet sheet = wb.getSheetAt(i);     //creating a Sheet object to retrieve object  
                Iterator<Row> itr = sheet.iterator();    //iterating over excel file
                int rowCounter = 0;
                int cellCounter = 0;
                ArrayList<String> fieldNames = new ArrayList<>();
                while (itr.hasNext()) {  
                    Row row = itr.next();
                    if (row.getRowNum()==0 || row.getRowNum()==2){
                        Iterator<Cell> cellIterator = row.cellIterator();   //iterating over each column 
                        int fieldCounter = 0;
                        while (cellIterator.hasNext() ) {  
                            Cell cell = cellIterator.next();
                            if (row.getRowNum()!=0 && fieldCounter!=0){
                                String cellvalue = "";
                                if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
                                    cellvalue = cell.getNumericCellValue() +"";
                                else
                                    cellvalue = cell.getStringCellValue();                                 
                                
                                if (i==0 && fieldCounter-1<32){ // MOTHER
                                    Element child = resource.addElement(Resources.MOTHERFIELDS.get(fieldCounter-1));
                                    child.setText(cellvalue);
                                } else if (i==1){ // CHILD
//                                    System.out.println(Resources.CHILDFIELDS.get(fieldCounter-1));
                                    Element child = resource.addElement(Resources.CHILDFIELDS.get(fieldCounter-1));
                                    child.setText(cellvalue);
                                } else if (i==2){ // GRANDCHILD
//                                    System.out.println(Resources.GRANDCHILDFIELDS.get(fieldCounter-1));
                                    Element child = resource.addElement(Resources.GRANDCHILDFIELDS.get(fieldCounter-1));
                                    child.setText(cellvalue);
                                }
                            }
                            fieldCounter++;
                        }
                        

                    }
                }
                
                if (i==1){ // ADD MONUMENT ID
                    String filename = docs.get(0).getRootElement().element("row").element("filename").getText();
                    Element child = resource.addElement(Resources.MONUMENT_ID);
                    child.setText(filename);
                    String includes = docs.get(0).getRootElement().element("row").element("Includes").getText().toLowerCase();
                    String title = docs.get(1).getRootElement().element("row").element("Title").getText().trim().toLowerCase();
                    Matcher m = Pattern.compile("([0-9]+)+\\.*\\s*"+title).matcher(includes);
                    if (m.find()){
                        Element child2 = resource.addElement(Resources.CHILD_ID);
                        child2.setText(m.group(1));
                    } else {
                        Element child2 = resource.addElement(Resources.CHILD_ID);
                        child2.setText(title.substring(title.lastIndexOf("/")+1));
                    }
                } else if (i==2){ // ADD MONUMENT ID AND CHILD ID
                    String filename =docs.get(0).getRootElement().element("row").element("filename").getText();
                    Element child = resource.addElement(Resources.MONUMENT_ID);
                    child.setText(filename);
                    String child_id = docs.get(1).getRootElement().element("row").element(Resources.CHILD_ID).getText();
                    Element child2 = resource.addElement(Resources.CHILD_ID);
                    child2.setText(child_id);
                    String con = docs.get(2).getRootElement().element("row").element("Title").getText();
                    Matcher m2 = Pattern.compile("\\(\\s*([0-9a-zA-Z_]+)\\s*\\)").matcher(con);
                    if (m2.find()){
                        Element child3 = resource.addElement(Resources.GRANCHILD_ID);
                        child3.setText(m2.group(1));
                    } else {
                        Element child3 = resource.addElement(Resources.GRANCHILD_ID);
                        child3.setText("01");
                    }     
                }
            }
        } catch(Exception e) {  
            e.printStackTrace();  
        }
        return docs;
    }
    
}
