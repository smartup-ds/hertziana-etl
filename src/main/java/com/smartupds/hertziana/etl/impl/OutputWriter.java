/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.hertziana.etl.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author mafragias
 */
public class OutputWriter {
    private final String outputPath;
    public OutputWriter(String outputPath){
        File file = new File(outputPath);
        file.getParentFile().mkdirs();
        this.outputPath = outputPath;
    }
    public void write(Document doc) throws IOException {
        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter xmlwriter = new XMLWriter( new OutputStreamWriter(new FileOutputStream(outputPath), "UTF-8"), format );
        xmlwriter.write(doc);
        xmlwriter.close();
    }
    
}
