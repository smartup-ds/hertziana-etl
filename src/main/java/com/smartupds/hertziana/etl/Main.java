/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.hertziana.etl;

import com.smartupds.hertziana.etl.impl.OutputWriter;
import com.smartupds.hertziana.etl.impl.Parser;
import com.smartupds.hertziana.etl.impl.Transformer;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;

/**
 *
 * @author mafragias
 */
public class Main {
    static final CommandLineParser PARSER = new DefaultParser();
    static Options options = new Options();
        
    public static void main(String[] args) throws ParseException, FileNotFoundException, DocumentException, IOException {
        createOptionsList();
//        String[] fakeArgs = {   "-c",
//                                "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\sacred_spaces_child.xml",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\sacred_spaces_child_transformed.xml"};
//        String[] fakeArgs = {   "-g",
//                                "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\sacred_spaces_grandchild.xml",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\sacred_spaces_grandchild_transformed.xml"};
//        String[] fakeArgs = {   "-m",
//                                "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\sacred_spaces_mother.xml",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\sacred_spaces_mother_transformed.xml"};
//        String[] fakeArgs = {   "-mothers",
//                                "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\mothers.xml",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\mothers_transformed.xml"};
//        String[] fakeArgs = {   "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\provinces.tsv",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\provinces.xml"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\ABR\\ABR_PE_Pianella_SMariaMaggiore.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\ABR_transformed\\ABR_PE_Pianella_SMariaMaggiore"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\PUG\\PUG_BA_Bari_Cattedrale.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\PUG_transformed\\PUG_BA_Bari_Cattedrale"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\SIC\\SIC_PA_Palermo_CappellaPalatina.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\SIC_transformed\\SIC_PA_Palermo_CappellaPalatina"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\SIC\\SIC_ME_Messina_SSalvatoreLingua.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\SIC_transformed\\SIC_ME_Messina_SSalvatoreLingua"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAM\\CAM_NA_Nola_Cattedrale.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAM_transformed\\CAM_NA_Nola_Cattedrale"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAM\\CAM_SA_Capaccio_MadonnaGranato.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAM_transformed\\CAM_SA_Capaccio_MadonnaGranato"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAM\\CAM_SA_Salerno_Cattedrale.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAM_transformed\\CAM_SA_Salerno_Cattedrale"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAL\\CAL_CS_Rossano_SMariaPatir.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAL_transformed\\CAL_CS_Rossano_SMariaPatir"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAL\\CAL_RC_Reggio_SMariaTerreti.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\CAL_transformed\\CAL_RC_Reggio_SMariaTerreti"};
        
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB\\SIC_PA_Monreale_ComplessodellaCattedralediMonreale_CattedralediSMariaNuova.xlsx",
//                                    "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB_transformed\\SIC_PA_Monreale_ComplessodellaCattedralediMonreale_CattedralediSMariaNuova"};
        
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB\\CAM_SA_Amalfi_ComplessodellaCattedralediAmalfi_CattedralediSAndreaApostolo.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB_transformed\\CAM_SA_Amalfi_ComplessodellaCattedralediAmalfi_CattedralediSAndreaApostolo"};

//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB\\CAM_SA_Amalfi_ComplessodellaCattedralediAmalfi_ChiostrodelParadiso.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB_transformed\\CAM_SA_Amalfi_ComplessodellaCattedralediAmalfi_ChiostrodelParadiso"};
//        args = new String[]{    "-f","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB\\CAM_SA_Amalfi_ComplessodellaCattedralediAmalfi_BasilicadelCrocifisso.xlsx",
//                                "-o","C:\\Users\\mafragias\\Documents\\Hertziana-SacredSpaces\\SCHEDE Database (Mother-Child-Grandchild)\\HUB_transformed\\CAM_SA_Amalfi_ComplessodellaCattedralediAmalfi_BasilicadelCrocifisso"};

        CommandLine cli = PARSER.parse(options, args);
        Parser parser = new Parser(cli.getOptionValue("f"));
        Document doc = DocumentHelper.createDocument();
        ArrayList<Document> docs = new ArrayList<>(); 
        if (cli.getOptionValue("f").substring(cli.getOptionValue("f").lastIndexOf(".")+1).equalsIgnoreCase("xml"))
            doc = parser.parseXML();
        else if (cli.getOptionValue("f").substring(cli.getOptionValue("f").lastIndexOf(".")+1).equalsIgnoreCase("xml"))
            doc = parser.parseTSVtoXML();
        else
            docs = parser.parseExcelToXML();
        

        if (docs.size()>0){
            int counter =0;
            for (Document doc_:docs){
                OutputWriter writer = new OutputWriter(cli.getOptionValue("o")+"_"+counter+"_original.xml");
                writer.write(doc_);
                counter++;
            }
            for (int j=0;j<docs.size();j++){
                switch (j) {
                    case 0:
                        {
                            Transformer transformer = new Transformer(docs.get(j));
                            transformer.transformMother();
                            break;
                        }
                    case 1:
                        {
                            Transformer transformer = new Transformer(docs.get(j));
                            transformer.transformChild();
                            break;
                        }
                    case 2:
                        {
                            Transformer transformer = new Transformer(docs.get(j));
                            transformer.transformGrandChild();
                            break;
                        }
                    default:
                        break;
                }
            }
        } else {
            Transformer transformer = new Transformer(doc);
            if (cli.hasOption("m")){
                transformer.transformMother();
            } else if (cli.hasOption("mothers")){
                transformer.transformMothers();
            } else if (cli.hasOption("c")) {
                transformer.transformChild();
            } else if (cli.hasOption("g")) {
                transformer.transformGrandChild();
            }
        }
        if (docs.size()>0) {
            int counter =0;
            for (Document doc_:docs){
                OutputWriter writer = new OutputWriter(cli.getOptionValue("o")+"_"+counter+"_transformed.xml");
                writer.write(doc_);
                counter++;
            }
        } else{
            OutputWriter writer = new OutputWriter(cli.hasOption("o")?cli.getOptionValue("o"):"./");
            writer.write(doc);
        }
        
    }
    
    private static void createOptionsList(){
        Option fileOption = new Option("f", true,"The XML file");
        fileOption.setRequired(true);
        Option motherOption = new Option("m", "mother", false,"Specify type of file as mother");
        Option mothersOption = new Option("mothers", "mothers", false,"Specify type of file as mother");
        Option childOption = new Option("c", "child", false,"Specify type of file as child");
        Option grandchildOption = new Option("g", "grandchild", false,"Specify type of file as grandchild");
        Option outputOption = new Option("o", "output", true,"Specify output");
        options.addOption(fileOption)
               .addOption(motherOption)
               .addOption(mothersOption)
               .addOption(childOption)
               .addOption(grandchildOption)
               .addOption(outputOption);
    }
}
